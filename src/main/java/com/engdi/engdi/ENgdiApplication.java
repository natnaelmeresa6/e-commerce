package com.engdi.engdi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ENgdiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ENgdiApplication.class, args);
	}

}
